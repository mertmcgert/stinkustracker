var Tracker = require('../models/trackerModel');

module.exports = function (app) {
    // zachs super secret button until matt makes the UI
    app.get('/zachlovestrannydicks', function(req, res) {
        var button = '<form action="/api/add" method="post"><button name="foo" value="/api/add">Stinkus made another stinkus</button></form>';

        res.send(button);
    });

    app.get('/', function(req, res) {
        res.send("( . Y . )");
    });

    app.get('/api/getPoops', function(req, res){
        Tracker.count({}, function(err, count){
            console.log( "Number of docs: ", count );
            res.send("" +count);
        });
    });

    app.get('/api/getPoops/:year', function(req, res) {
        var year = req.params.year;

        var dateMin = new Date(String(+year));
        var dateMax = new Date(String(+year+1));
        var query = {
            poopDate: {
                $gte: dateMin,
                $lt: dateMax
            }
        };

        console.log( "Date: ", dateMin);
        Tracker.count(query, function(err, count){
            console.log( "Number of docs: ", count );
            res.send("" +count);
        });
    });

    app.get('/api/getPoops/:year/:month', function(req, res) {
        var year = req.params.year;
        var month = req.params.month;

        var dateMin = new Date(year, String(+month-1));
        var dateMax = new Date(year, String(+month));
        var query = {
            poopDate: {
                $gte: dateMin,
                $lt: dateMax
            }
        };

        console.log( "Date: ", dateMin);
        Tracker.count(query, function(err, count){
            console.log( "Number of docs: ", count );
            res.send("" +count);
        });
    });

    app.get('/api/getPoops/:year/:month/:day', function(req, res) {
        var year = req.params.year;
        var month = req.params.month;
        var day = req.params.day;

        var dateMin = new Date(year, String(+month-1), String(+day));
        var dateMax = new Date(year, String(+month-1), String(+day+1));
        var query = {
            poopDate: {
                $gte: dateMin,
                $lt: dateMax
            }
        };

        console.log( "Date: ", dateMin);
        Tracker.count(query, function(err, count){
            console.log( "Number of docs: ", count );
            res.send("" +count);
        });
    });

    app.post('/api/add/', function (req, res) {
        var track = new Tracker();
        track.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Stinkus stunk it up!' });
        });
    });

    app.get('/:pageCalled', function(req, res) {
        console.log('retrieving page: ' + req.params.pageCalled);

        var message = req.params.pageCalled + " is not a page, stupid.";
        message += "<br><br>/api/getPoops";
        message += "<br>/api/getPoops/:year";
        message += "<br>/api/getPoops/:year/:month";
        message += "<br>/api/getPoops/:year/:month/:day";

        res.send(message);
    });

};